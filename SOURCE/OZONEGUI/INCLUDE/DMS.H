////////////////////////////////////////////////////////////////////////////////
//
//	Data Management System - Haeder file
//
//	See also types.h for definitions
//
//	(c) Copyright 2003,2004 Point Mad, Lukas Lipka. All rights reserved.
//
//	Documentation: docs/html/dms.htm
//
////////////////////////////////////////////////////////////////////////////////
#ifndef _DMS_H_INCLUDED_
#define _DMS_H_INCLUDED_

#include "allegro.h" // Needed for BITMAP structure
#include "types.h"
#include "list.h"
#include "vfile.h"
#include "resource.h"

/**
*	Data Codec definition
*/
typedef struct TCodec *PCodec;
typedef struct TCodec
{
	l_datatype		DataTypeManaged;    // Main type (ex : TYPE_IMAGE)
	l_dataencode	DataEncodeManaged;  // Encoding type, Use ULONG_ID('?','?','?','?') (ex: Bmp, Jpg, Png...), NULL -> ALL

	l_text			Name;      // Encoder name (ex: "Windows Bitmap","JPEG","Portable Network Graphic")
	l_text			Extention; // General associated extention (ex : "BMP","JPG","PNG"), NULL -> ALL

	l_bool			UseForRessource;

	l_bool			(*SaveToFile)( l_text file, l_ptr  Data, l_ulong  Size );
	l_bool			(*LoadFromFile)( l_text file, l_ptr *Data, l_ulong *Size );

	l_bool			(*Save)( PFile file, l_ptr  Data, l_ulong  Size, l_ulong *EndOffset );
	l_bool			(*Load)( PFile file, l_ptr *Data, l_ulong *Size, l_ulong *EndOffset );

	l_int			*ExtraOptions;
	l_bool			(*ExtraOptionsSet)( PCodec o );

} TCodec;

typedef struct TDataMan *PDataMan;
typedef struct TDataMan
{

	l_datatype	Type;
	l_text			Name;

	void			(*FreeData)(void*);
	void* 		(*Duplicate)(void*,l_ulong);
} TDataMan;


#define DATAMAN(o) ((PDataMan)(o))
#define CODEC(o) ((PCodec)(o))


PCodec InstallNewCodec ( l_datatype   DataTypeManaged,
	                           l_dataencode DataEncodeManaged,
	                           l_text Name,
	                           l_text Extention,
	                           l_bool UseForRessource,
	                           l_bool (*SaveToFile)   ( l_text file, l_ptr  Data, l_ulong  Size ),
	                           l_bool (*LoadFromFile) ( l_text file, l_ptr *Data, l_ulong *Size ),
	                           l_bool (*Save) ( PFile file, l_ptr  Data, l_ulong  Size, l_ulong *EndOffset ),
	                           l_bool (*Load) ( PFile file, l_ptr *Data, l_ulong *Size, l_ulong *EndOffset ),
	                           l_bool (*ExtraOptionsSet) ( PCodec o ) );
void UnInstallCodec ( PCodec o );
PCodec FoundCodecByEncoder ( l_datatype DataTypeAccepted, l_dataencode DataEncode );
PCodec FoundCodecByExtention ( l_datatype DataTypeAccepted, l_text Extention );
PCodec DefaultRessourceCodec ( l_datatype DataTypeAccepted );
PCodec DefaultCodec ( l_datatype DataTypeAccepted );

void InitDMS ( void );
void ShutDownDMS ( void );


l_text GetFileExtention ( l_text File );

l_bool SaveSizedData ( l_datatype Type, void *Data, l_ulong Size, l_text File );
l_bool SaveData ( l_datatype Type, void *Data, l_text File );
void 	*LoadData ( l_datatype ExecptedType, l_text File );
void 	*LoadSizedData ( l_datatype ExecptedType, l_text File, l_ulong *Size );


PDataMan InstallNewDataManager (l_datatype Type,l_text Name,void (*FreeData)(void*),void*(*Duplicate)(void*,l_ulong) );
void DataFree ( l_ulong Type, void *Data );
void *DataDuplicate ( l_ulong Type, void *Data, l_ulong Size );


#endif /* _DMS_H_INCLUDED_ */
