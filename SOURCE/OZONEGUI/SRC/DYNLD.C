////////////////////////////////////////////////////////////////////////////////
//
//	Dynamic Loading System - Loader
//
//	(c) Copyright 2003,2004 Point Mad. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////
#define	 KERNEL_FILE
#include "kernel.h"
#include "dms.h"

l_bool UnLoadLibraries ( l_uid * );

/**
*	Global variables
*/
PList Symbols		= 0;
PList Applications	= 0;

/**
*	Kernel indetification
*/
TApplication Me =
{
	"Codename: Phoenix Kernel",
	"",
	ULONG_ID(0,5,2,0),
	NULL,
	NULL,
	NULL,
	NULL,
	0,
	0,
	0,
	"kernel",
	0,
	1
};

void (*ExternConsolePreInit) ( PApplication App );
void (*ExternConsolePreUnInit) ( PApplication App );

/**
*	NAME: AddSymbol
*	DESCRIPTION: Used by the macros to add or export a symbol
*	RETURN: The exported symbol
*/
PSymbol AddSymbol ( l_text Name, void *Address, PApplication App )
{
	PSymbol o;

	if ( o = (PSymbol)ListKeyItem(Symbols, Name) )
	{
		DebugWarning("Duplicate symbol: %s orignaly registered by %x", Name, o->App);
		return 0;
	}

	o = NEW(TSymbol);

	if ( !o || !Name ) return NULL;

	memset(o, 0, sizeof(TSymbol));
	o->Itm.Key		= (l_text)strdup(Name);
	o->Itm.Data		= Address;
	o->Itm.FreeData	= NULL;
	o->App			= App;

	ListAddItem(Symbols, (PListItem)o);

	return o;
}

/**
*	NAME: ResolveSymbol
*	DESCRIPTION: Resolves the symbol in a DynLD app
*	RETURN: The address of the symbol
*/
l_long ResolveSymbol ( PApplication App, l_text Name )
{
	l_long Address = 0;

	if ( !strcmp(Name, "Me") )
		Address = (l_long)App;
	else
		Address = (l_long)ListKey(Symbols, Name);

	if ( !Address )
	{
		DebugError ("0x%08x :: Unresolved external %s", App, Name);
	}

	return Address;
}

void FreeApplication ( void *o )
{
	PApplication App = (PApplication)o;

	DebugMessage("Free Application :: %s",App->Name);

	if ( App->Close ) App->Close();

	//if ( App->ThreadId ) KillThread(App->ThreadId);

	if ( App->Name )     free(App->Name);
	if ( App->FileName ) free(App->FileName);
	if ( App->Ressource ) FreeList(App->Ressource);

	free(App->Data);

/* ** Due to unknown threading bugs, this section is unactivated **

	FreeList(App->EventsWaitList);

	*/

	free(App);
}
////////////////////////////////////////////////////////////////////////////////
l_bool ShutDownApplication ( PApplication App )
{
	if (!App) return 0;

	DebugMessage("ShutDown %s",App->Name);

	if ( Symbols->Last )
	{
		PListItem a, b, n;

		a = b = Symbols->Last;

		if ( a )
			do
			{
				n = a->Next;

				if ( SYMBOL(a)->App == App )
				{
					ListRemoveItem(Symbols, a);
				}

				a = n;
			} while ( a != b );
	}

	if ( Applications ->Last )
	{
		PListItem a = ListFoundItem(Applications, App);

		if ( a )
		{
			ListRemoveItem(Applications,a);
		}
	}

	UnLoadLibraries(App->Libs);

	return true;
}

/*l_text ReadString ( FILE *f ) >>> vfile.c
{
	l_ushort length	= 0;
	l_text  s	= 0;

	FileRead(&length, 1, 2, f);
	s = (l_text)malloc(length+1);
	if ( !s ) return NULL;
	FileRead(s, 1, length, f);
	s[length] = 0;

	return s;
}*/
////////////////////////////////////////////////////////////////////////////////
PApplication GetLibrary ( l_uid id ) {

	if ( Applications->Last )
	{
		PListItem a = Applications->Last;
		PListItem b = a;
		PApplication App;
		do
		{

			App = (PApplication)(a->Data);
			if ( !TextCompare(App->UID,id) )
			{
				return App;
			}

			a = a->Next;
		} while ( a != b );
	}
	return NULL;
}
////////////////////////////////////////////////////////////////////////////////
l_bool LoadLibrary ( l_uid id ) {
	PApplication app = GetLibrary(id);
	if ( !app ) {
		l_text key = TextArgs("/SYSTEM/LIBRARIES/%s",id);
		if ( key ) {
			l_text file = KeyGetText(key,NULL);
			free(key);
			if ( !file ) {
				file = TextArgs("/system/xlib/%s.dl",id);
				if ( !file ) return false;
			}
			app = DynLdRun(file,NULL);
			if ( !app ) return false;
		} else
			return false;
	} else
		app->Users++;
	return true;
}
////////////////////////////////////////////////////////////////////////////////
l_bool UnLoadLibrary ( l_uid id ) {
	PApplication app = GetLibrary(id);

	if ( !app ) return false;
	app->Users--;

	if ( !app->Users ) CloseApp(app);

	return true;
}
////////////////////////////////////////////////////////////////////////////////
l_bool LoadLibraries ( l_uid *ids ) {
	DebugMessage("LoadLibraries %x",ids);
	while ( (*ids)[0] ) {
		DebugMessage("> %s",ids);
		if ( !LoadLibrary(*ids) ) return false;
		ids ++;
	}
	return true;
}
////////////////////////////////////////////////////////////////////////////////
l_bool UnLoadLibraries ( l_uid *ids ) {
	DebugMessage("UnLoadLibraries %x",ids);
	while ( (*ids)[0] ) {
		DebugMessage("> %s",ids);
		if ( !UnLoadLibrary(*ids) ) DebugWarning("Unable to unload '%s'",ids);
		ids ++;
	}
	return true;
}
////////////////////////////////////////////////////////////////////////////////
PApplication DynLdRun ( l_text Filename, l_text Args )
{
	PApplication App		= 0;
	void *TempData			= 0;
	PDynLdExt Ext			= 0;
	l_ulong i 				= 0;
	l_ulong DynERROR		= 0;
	TDynLdReloc DynReloc;
	TDynLdHeader DynHead;

	/*
	*	Open application file
	*/
	PFile f = FileOpen(Filename, "rb");

	if ( !f )
	{
		DebugError("DYNLD :: File not found - %s", Filename);
		return NULL;
	}

	FileRead(&DynHead, 1, sizeof(TDynLdHeader), f);

	if ( DynHead.Magic != ULONG_ID('D','n','L','d') )
	{
		FileClose(f);
		DebugError("DYNLD :: Application is not a valid DynLD.");

		return NULL;
	}

	if ( DynHead.FileFormatVersion > DYNLDVERSION )
	{
		FileClose(f);
		DebugError("DYNLD :: Application has invalid version number of DynLD linker.");

		return NULL;
	}

	App = (PApplication)malloc(sizeof(TApplication));

	if ( !App ) {
		FileClose(f);
		DebugError("DYNLD :: Not enough memory to load application.");
		return NULL;
	}

	memset(App, 0, sizeof(TApplication));

	App->Name           = FileReadBinString(f);
	App->FileName       = (l_text)strdup(Filename);
	memcpy(&App->UID,&DynHead.UID,sizeof(l_uid));
	App->Version        = DynHead.FileVersion;
	/* ** Due to unknown threading bugs, this section is unactivated **

	App->EventsWaitList = NewList();
	*/
	App->Type           = DynHead.Type;

	DebugMessage ("Loading application '%s'(%x), FileName - '%s'",App->Name,App,App->FileName);
	DebugMessage ("         UID - '%s' Version - 0x%08x",App->UID,App->Version);

	/*if ( DynHead.Compression != 0)
	{
		TempData = (void*)malloc(DynHead.Size);
		FileRead(TempData, 1, DynHead.Size, f);

		App->Data = (void*)malloc(DynHead.OriginalSize);
		uncompress(App->Data, &DynHead.OriginalSize, TempData, DynHead.Size);
		DynHead.Size = DynHead.OriginalSize;
		free(TempData);
	}
	else
	{*/
		App->Data = (void*)malloc(DynHead.Size);
		FileRead(App->Data, 1, DynHead.Size, f);
	//}

	if ( DynHead.MainOffset  != NoneOffset ) App->Main  = (void*)(((long)App->Data)+DynHead.MainOffset);
	if ( DynHead.CloseOffset != NoneOffset ) App->Close = (void*)(((long)App->Data)+DynHead.CloseOffset);
	if ( DynHead.LibsOffset  != NoneOffset ) App->Libs  = (void*)(((long)App->Data)+DynHead.LibsOffset);


	if ( App->Libs )
		if ( !LoadLibraries(App->Libs) ) {
			free(App->Data);
			free(App);
			FileClose(f);
			return NULL;
		}


	Ext = (PDynLdExt)malloc(sizeof(TDynLdExt)*DynHead.Importations);


	for (i = 0; i <DynHead.Importations; i++)
	{
		Ext[i].Name = FileReadBinString(f);
		FileRead(&Ext[i].Symbol, 1, 4, f);
	}

	for (i = 0; i < DynHead.Relocations; i++)
	{
		FileRead(&DynReloc, 1, sizeof(TDynLdReloc), f);

		if (DynReloc.Type == 6)
		{ // Absolute reference
			l_ulong j, addr = (l_ulong)App->Data;

			for (j = 0; j<DynHead.Importations; j++)
				if (Ext[j].Symbol == DynReloc.Symbol)
				{
					addr = (l_ulong)ResolveSymbol(App,Ext[j].Name);
					if (!addr) DynERROR++;
					//else DebugMessage ("Resolved %s 0x%08x",Ext[j].Name,addr);

					break;
				}

			*(long*)(((((long)App->Data))+DynReloc.Address)) += addr;
		}
		else
		{ // Relative reference
			l_ulong j, addr = 0;

			for (j=0;j<DynHead.Importations;j++)
				if (Ext[j].Symbol == DynReloc.Symbol)
				{
					addr = (l_ulong)ResolveSymbol(App,Ext[j].Name);
					if (!addr) DynERROR++;

					// else DebugMessage ("Resolved %s 0x%08x",Ext[j].Name,addr);

					break;
				}

			*(long*)((((long)App->Data))+DynReloc.Address) += (long)addr-(long)(App->Data);
		}
	}

	if ( DynHead.RessourceEntries )
		App->Ressource = DMSLoadRessource(f,DynHead.RessourceEntries);

	FileClose(f);

	for (i=0;i<DynHead.Importations;i++) free(Ext[i].Name);

	free(Ext);

	if ( DynERROR )
	{
		App->Close = NULL;
		DebugError ("0x%08x :: %d error(s) while loading. Application has not started", App, DynERROR);
		FreeApplication(App);
		return NULL;
	}

	/* ** Due to unknown threading bugs, this section is unactivated **

	if ( App->Type == DYNLD_TYPEAPP )
	{
		// Stack = 10k
		App->ThreadId = NewThread((void*)App->Main,Args,10240,1);
		//DebugMessage ("         Thread - 0x%08x",App->ThreadId);

		while ( App->State == DynLdStateStarting ) Yield();

		//DebugMessage ("         Started!");
	}
	else
	{*/
	DebugMessage("ExternConsolePreInit");

	if ( ExternConsolePreInit ) ExternConsolePreInit(App);

	DebugMessage("Calling @App->Main(Args)");

  if ( App->Main )
	  if ( !App->Main(Args) ) {
			App->Close = NULL;
			DebugMessage ("0x%08x :: ExternConsolePreUnInit");
			if ( ExternConsolePreUnInit ) ExternConsolePreUnInit(App);
			DebugMessage ("0x%08x :: Application breaked");
			FreeApplication(App);
			return NULL;
		}
	//}

	App->Users = 1;

	DebugMessage("Adding app to list");

	ListAdd(Applications,NULL,App,FreeApplication);


	return App;
}
////////////////////////////////////////////////////////////////////////////////

/* ** Due to unknown threading bugs, this section is unactivated **

void AppSendEvent ( PApplication App, l_ulong Type, l_ulong Msg, void *Extra, void *Dest, void (*Redirect) ( void *, PDynLdEvent ) )
{
	PDynLdEvent Ev = (PDynLdEvent)malloc(sizeof(TDynLdEvent));

	Ev->Ev.Type    = Type;
	Ev->Ev.Message = Msg;
	Ev->Ev.Extra   = Extra;
	Ev->Dest     = Dest;
	Ev->Redirect = Redirect;

	ListAdd(App->EventsWaitList,NULL,Ev,&free);
}

void AppSendEventEv ( PApplication App, TEvent Ev, void *Dest, void (*Redirect) ( void *, PDynLdEvent ) )
{
	PDynLdEvent Evt = (PDynLdEvent)malloc(sizeof(TDynLdEvent));

	Evt->Ev  = Ev;

	Evt->Dest     = Dest;
	Evt->Redirect = Redirect;

	ListAdd(App->EventsWaitList,NULL,Evt,&free);
}

l_bool AppGetNextEvent ( PApplication App, PDynLdEvent Ev )
{
	while (!App->EventsWaitList->Last) Yield();

	memcpy(Ev, App->EventsWaitList->Last->Next->Data, sizeof(TDynLdEvent));

	ListRemoveItem(App->EventsWaitList, App->EventsWaitList->Last->Next);

	if ( Ev->Ev.Message == MSG_KILLAPP && Ev->Ev.Type == EV_MESSAGE ) return false;

	return true;
}

void AppRedirectEvent ( PApplication App, PDynLdEvent Ev )
{
	if ( Ev->Redirect ) Ev->Redirect(Ev->Dest, Ev);
}*/
////////////////////////////////////////////////////////////////////////////////
void CloseApp ( PApplication App ) {

	DebugMessage ("Close :: %s",App->Name);

  	if ( App->Close )
	{

		App->Close();
		App->Close = NULL;
	}
	DebugMessage("ExternConsolePreUnInit");
	if ( ExternConsolePreUnInit ) ExternConsolePreUnInit(App);

	if ( Symbols->Last )
	{
		PListItem a, b, n;

		a = b = Symbols->Last->Next;

		do
			{
				n = a->Next;

				if ( SYMBOL(a)->App == App )
				{
					ListRemoveItem(Symbols, a);
					if ( a == b ) b = Symbols->Last->Next;
				}

				a = n;
			} while ( a != b );
	}
	App->State = DynLdStateToFree; // App will be free by DYNLDTCP Thread


}
////////////////////////////////////////////////////////////////////////////////
PTask DynLdTCPId = 0;
////////////////////////////////////////////////////////////////////////////////
void DynLdTCP ( PTask o ) {

	if ( Applications->Last )
	{
		PListItem a = Applications->Last->Next;
		PListItem b = a,n;
		PApplication App;

		do
		{
			n = a->Next;
			App = (PApplication)(a->Data);
			if ( App->State == DynLdStateToFree )
			{
				if ( App->Libs ) UnLoadLibraries(App->Libs);
				ListRemoveItem(Applications, a);
				if ( a == b ) b = Applications->Last->Next;
			}

			a = n;
		} while ( a != b );
	}




}
////////////////////////////////////////////////////////////////////////////////
l_bool DynLdInstallLibrary ( l_text file ) {
	l_text key,n;
	TDynLdHeader DynHead;
	PFile f = FileOpen(file, "rb");

	if ( !f )
	{
		DebugError("DYNLD :: File not found - %s", file);
		return false;
	}

	FileRead(&DynHead, 1, sizeof(TDynLdHeader), f);
	if ( DynHead.Magic != ULONG_ID('D','n','L','d') )
	{
		FileClose(f);
		DebugError("DYNLD :: Library is not a valid DynLD. Library not installed.");
		return false;
	}

	if ( DynHead.FileFormatVersion > DYNLDVERSION )
	{
		FileClose(f);
		DebugError("DYNLD :: Library has invalid version number of DynLD linker. Library not installed.");
		return false;
	}

	FileClose(f);

	key = TextArgs("/SYSTEM/LIBRARIES/%s",DynHead.UID);
	if ( !key ) return false;
	n 	= TextArgs("%s",DynHead.UID);
	if ( !n ) return false;

	NewKey("/SYSTEM/LIBRARIES",n);

	KeySetText(key,file);

	free(key);
	free(n);


	return true;
}
////////////////////////////////////////////////////////////////////////////////
void InitDynLd ( void )
{
	Symbols = NewList();

	if ( !Symbols ) DebugFatal("DynLd::ERROR_NOTENOUGHMEMORY");

	Applications = NewList();

	if ( !Applications ) DebugFatal("DynLd::ERROR_NOTENOUGHMEMORY");

	SYSEXPORT(AddSymbol);
	SYSEXPORT(CloseApp);
	SYSEXPORT(ResolveSymbol);
	SYSEXPORT(DynLdInstallLibrary);

	SYSEXPORT(ExternConsolePreInit);
	SYSEXPORT(ExternConsolePreUnInit);
	/* ** Due to unknown threading bugs, this section is unactivated **

	SYSEXPORT(AppSendEvent);
	SYSEXPORT(AppSendEventEv);
	SYSEXPORT(AppGetNextEvent);
	SYSEXPORT(AppRedirectEvent);*/

	DynLdTCPId = InstallTask(DynLdTCP);


}

void ShutDownDynLd ( void )
{
	DebugMessage ("Shutting down DynLd...");

	RemoveTask(DynLdTCPId);


	if ( Applications->Last )
	{
		PListItem a = Applications->Last;
		PListItem b = a;
		PApplication App;

		do
		{
			App = (PApplication)(a->Data);

			/* ** Due to unknown threading bugs, this section is unactivated **

			if ( App->Type == DYNLD_TYPEAPP )
			{
				DebugMessage("Kill thread :: 0x%08x : %s : %d", App, App->Name, App->ThreadId);
				AppSendEvent(App, EV_MESSAGE, MSG_KILLAPP, NULL, NULL, NULL);

				while ( App->State != DynLdStateDied ) Yield();

				KillThread(App->ThreadId);

				App->ThreadId = 0;
				App->Close = NULL;
			}
			else */if ( App->Close )
			{
				DebugMessage ("Close :: %s",App->Name);

				App->Close();
				App->Close = NULL;

			}
				DebugMessage("ExternConsolePreUnInit");
			if ( ExternConsolePreUnInit ) ExternConsolePreUnInit(App);

			a = a->Prev;
		} while ( a != b );
	}

	FreeList(Symbols);
	FreeList(Applications);

	DebugMessage ("DynLd Stopped");
}
////////////////////////////////////////////////////////////////////////////////
