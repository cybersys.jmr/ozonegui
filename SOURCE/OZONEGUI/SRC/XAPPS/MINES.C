/**
 * TODO:
 *		+ reveal all 0 fields
 *		+ count left mines
 *		+ timer for score
 */
#include "kernel.h"
#include "canvas.h"
#include "button.h"
#include "window.h"
#include "timer.h"
#include "menu.h"

l_uid 	nUID 		= "ll:o3mines";
l_ulong AppVersion	= ULONG_ID(0,0,1,0);
char AppName[]		= "03Mines";
l_uid NeededLibs[]	= { "widget","window","menu","canvas","" };

#define FWIDTH	8
#define FHEIGHT	8
#define BSIZE	20
#define MINES	10

#define Q_BOMB		0x00000010
#define Q_QUESTION	0x00000008
#define Q_FLAGED	0x00000004
#define Q_VISITED	0x00000002

#define MSG_NEWGAME 0xdeadbeef

PWindow w = 0;
PCanvas MineField = 0;
l_ulong MineSource[FWIDTH][FHEIGHT];
l_bool End = 0;

void OpenMinesAround ( l_int x, l_int y )
{
	if ( x > 0 && y > 0 )
		MineSource[x - 1][y - 1] |= Q_VISITED;

	if ( y > 0 )
		MineSource[x][y - 1] |= Q_VISITED;

	if ( x < (FWIDTH-1) && y >= 0 )
		MineSource[x + 1][y - 1] |= Q_VISITED;

	if ( x > 0 )
		MineSource[x - 1][y] |= Q_VISITED;

	if ( x < (FWIDTH-1) )
		MineSource[x + 1][y] |= Q_VISITED;

	if ( x > 0 && y < (FHEIGHT-1) )
		MineSource[x - 1][y + 1] |= Q_VISITED;

	if ( y < (FHEIGHT-1) )
		MineSource[x][y + 1] |= Q_VISITED;

	if ( x < (FWIDTH-1) && y < (FHEIGHT-1) )
		MineSource[x + 1][y + 1] |= Q_VISITED;
}

l_int GetMinesAround ( l_int x, l_int y )
{
	l_int i = 0;

	if ( x > 0 && y > 0 )
		if (MineSource[x - 1][y - 1] & Q_BOMB) i++;

	if ( y > 0 )
		if (MineSource[x][y - 1] & Q_BOMB) i++;

	if ( x < (FWIDTH-1) && y >= 0 )
		if (MineSource[x + 1][y - 1] & Q_BOMB) i++;

	if ( x > 0 )
		if (MineSource[x - 1][y] & Q_BOMB) i++;

	if ( x < (FWIDTH-1) )
		if (MineSource[x + 1][y] & Q_BOMB) i++;

	if ( x > 0 && y < (FHEIGHT-1) )
		if (MineSource[x - 1][y + 1] & Q_BOMB) i++;

	if ( y < (FHEIGHT-1) )
		if (MineSource[x][y + 1] & Q_BOMB) i++;

	if ( x < (FWIDTH-1) && y < (FHEIGHT-1) )
		if (MineSource[x + 1][y + 1] & Q_BOMB) i++;

	return i;
}

void FieldDraw ( PWidget o, p_bitmap buffer, PRect w )
{
	l_int x = 0;
	l_int y = 0;

	rectfill(buffer, WIDGET(MineField)->Absolute.a.x, WIDGET(MineField)->Absolute.a.y, WIDGET(MineField)->Absolute.b.x, WIDGET(MineField)->Absolute.b.y, makecol(200,200,200));

	do
	{
		do
		{
			if ( MineSource[x][y] & Q_VISITED )
			{
				rectfill(buffer, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+-1+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y-1+(y+1)*BSIZE, makecol(255,255,255));
				if (GetMinesAround(x,y))
					textout(buffer, default_font, TextArgs("%i", GetMinesAround(x,y)), WIDGET(MineField)->Absolute.a.x+x*BSIZE+(BSIZE/2), WIDGET(MineField)->Absolute.a.y+y*BSIZE, makecol(0,0,0));
			}
			else if ( MineSource[x][y] & Q_FLAGED )
			{
				rectfill(buffer, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+-1+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y-1+(y+1)*BSIZE, makecol(0,0,255));
				//textout F
			}
			else if ( MineSource[x][y] & Q_QUESTION )
			{
				rectfill(buffer, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y+(y+1)*BSIZE, makecol(200,200,200));
				//textout ?
			}
			else if ( (MineSource[x][y] & Q_BOMB) && End )
			{
					rectfill(buffer, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+-1+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y-1+(y+1)*BSIZE, makecol(255,0,0));
					rect(buffer, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+-1+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y-1+(y+1)*BSIZE, makecol(90,90,90));
			}
			else
				Rect3D(buffer, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+-1+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y-1+(y+1)*BSIZE, makecol(200,200,200), makecol(0,0,0));

			y++;
		}
		while ( y != FHEIGHT );

		y = 0; x++;
	}
	while ( x != FWIDTH );
}

void GetFieldUnder ( TPoint *p , TPoint m )
{
	l_int x = 0;
	l_int y = 0;

	do
	{
		do
		{
			TRect r;
			RectAssign(&r, WIDGET(MineField)->Absolute.a.x+x*BSIZE, WIDGET(MineField)->Absolute.a.y+y*BSIZE , WIDGET(MineField)->Absolute.a.x+(x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y+(y+1)*BSIZE);

			if ( PointInRect(m, r) )
			{
				p->x = x;
				p->y = y;
				return;
			}

			y++;

		}
		while ( y != FHEIGHT );

		y = 0; x++;
	}
	while ( x != FWIDTH );
}

FieldEH ( PWidget o, PEvent Event )
{
	if ( Event->Type == EV_MOUSE)
	{
		if ( Event->Message == WEvMouseLDown && !End )
		{
			TPoint p;
			GetFieldUnder(&p, Mouse->State.p);

			if (MineSource[p.x][p.y] & Q_FLAGED)
				return false;

			if (MineSource[p.x][p.y] & Q_BOMB)
			{
				End = true;
				WidgetDraw(WIDGET(MineField), NULL);
			}
			else if ( !MineSource[p.x][p.y] )
			{
				MineSource[p.x][p.y] |= Q_VISITED;
				//OpenMinesAround(p.x, p.y);
				WidgetDraw(WIDGET(MineField), NULL);
			}
		}

		if ( Event->Message == WEvMouseRDown && !End )
		{
			TPoint p;
			TRect r;

			GetFieldUnder(&p, Mouse->State.p);

			if (MineSource[p.x][p.y] & Q_FLAGED)
				MineSource[p.x][p.y] &= ~Q_FLAGED;
			else
				MineSource[p.x][p.y] |= Q_FLAGED;

			RectAssign(&r, WIDGET(MineField)->Absolute.a.x+p.x*BSIZE, WIDGET(MineField)->Absolute.a.y+p.y*BSIZE , WIDGET(MineField)->Absolute.a.x+(p.x+1)*BSIZE, WIDGET(MineField)->Absolute.a.y+(p.y+1)*BSIZE);
			WidgetDraw(WIDGET(MineField), &r);
		}
	}
	return false;
}

void NewRandomPoint ( TPoint *p )
{
	p->x = rand()%FWIDTH;
	p->y = rand()%FHEIGHT;

	if ( MineSource[p->x][p->y] == 1 )
	{
		NewRandomPoint(p);
		return;
	}
}

void GenerateMineSource ( void )
{
	l_int n = 0;
	TPoint p;

	memset(MineSource,0,sizeof(MineSource));

	while ( n != MINES )
	{
		NewRandomPoint(&p);

		MineSource[p.x][p.y] |= Q_BOMB;

		n++;
	}
}

l_bool AppEventHandler ( PWidget o, PEvent Event )
{
	if ( Event->Type == EV_MESSAGE )
	{
		switch ( Event->Message )
		{
			case WM_CLOSE:
			{
				CloseApp(&Me);
				return true;
			}
			break;

			case WM_ABOUT:
			{
				MessageBox(&Me, "About O3 Mines", "O3 Mines 0.1\nYet another cloned game for oZone GUI\n\nCopyright (c) 2004 Lukas Lipka. All rights reserved.", MBB_OK);
				return true;
			}
			break;

			case MSG_NEWGAME:
			{

				GenerateMineSource();
				End = false;
				WidgetDraw(WIDGET(MineField), NULL);

				return true;
			}
			break;
		}
	}

	return false;
}

l_int Main (l_text Args)
{
	TRect r;

	RectAssign(&r,0, 0, FWIDTH*BSIZE, FHEIGHT*BSIZE+25);
	w = CreateWindow(&Me, r, "O3 Mines", WF_CAPTION|WF_FRAME|WF_CENTERED|WF_MINIMIZE);
	InsertWidget(WIDGET(DeskTop), WIDGET(w));


	PMenu Menu = NewMenu(
     NewMenuItem( "Game", NULL, NULL, NULL,
     		 NewMenu (
     		 		NewMenuItem( "New game", NULL, MSG_NEWGAME, NULL, NULL,
     		 		NewMenuItemSeparator(
     		 		NewMenuItem( "Exit", NULL, WM_CLOSE, NULL, NULL,
     		 		NULL)))
     		 ),
     NewMenuItem( "Help", NULL, NULL, NULL,
     	NewMenu(
     		NewMenuItem( "About", NULL, WM_ABOUT, NULL, NULL, NULL)),
     NULL))
	);

	RectAssign(&r,0,0,FWIDTH*BSIZE,20);

	PMenuView o = NewMenuView(&Me,r,Menu,MenuViewStyleHorizontal,0);
	InsertWidget(WIDGET(w), WIDGET(o));

	WidgetSize(&r, 0, 25, FWIDTH*BSIZE, FHEIGHT*BSIZE);
	MineField = CreateCanvas(&Me, r);
	WIDGET(MineField)->Draw = &FieldDraw;
	WIDGET(MineField)->EventHandler = &FieldEH;
	WIDGET(MineField)->Flags |= WFForceBuffer;
	InsertWidget(WIDGET(w), WIDGET(MineField));

	GenerateMineSource();

	WidgetDrawAll(WIDGET(w));

	WIDGET(w)->AppEvHdl = &AppEventHandler;

	return true;
}

void Close (void)
{
	WidgetDispose(WIDGET(w));
}
