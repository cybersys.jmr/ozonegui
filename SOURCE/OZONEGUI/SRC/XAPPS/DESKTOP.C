////////////////////////////////////////////////////////////////////////////////
//
//	OZ Desktop
//
//	(c) Copyright 2003,2004 Point Mad, Lukas Lipka. All rights reserved.
//
////////////////////////////////////////////////////////////////////////////////
#include "kernel.h"
#include "widget.h"
#include "button.h"
#include "label.h"
#include "canvas.h"
#include "menu.h"
#include "list.h"
#include "window.h"
#include "fms.h"

#include "OZD/desktop.h"

#include <dos.h>
#include <math.h>

l_ulong AppVersion = ULONG_ID(0,0,1,0);
char    AppName[] = "OZ Desktop";
l_uid	nUID = "app:desktop";
l_uid NeededLibs[] = { "widget", "skin","button","label","menu","fms","window","grfx","" };


l_bool AppEventHandler ( PWidget o, PEvent Ev );

// Include the desktop files
#include "OZD/theme.c"
#include "OZD/wallpaper.c"
#include "OZD/panel.c"

void GenerateSubMenu ( PMenuItem o ,void* Args) {
 	l_text n; p_bitmap i16; l_ulong  fi;

	PMenuItem m = NULL;
	PListItem a, b;
	PList l;


	l = ListDir(Args,0);
	if ( l ) {
		if ( l->Last )
		{
			a = b = l->Last;
			if ( a )
			do
			{
				FileGetAllThingsEx(FILEINFO(a->Data),&n,&i16,NULL,NULL,&fi);

				if ( n[0] != '.' ){
					m = NewMenuItemEx(n,i16,IDM_PROGRAMS,0,NULL,NULL,TextDup(FILEINFO(a->Data)->VPathName),m );
					free(n);
				}

				a = a->Prev;
			}
			while ( a != b );
		}
		FreeList(l);
	}

	l = ListDir(Args,FA_DIREC|FA_LABEL);

	if ( l ) {;
		if ( l->Last )
		{
			a = b = l->Last;
			if ( a )
			do
			{
				if ( FILEINFO(a->Data)->Attributes & FA_DIREC )
				m = NewMenuItemEx(FILEINFO(a->Data)->Name,ICONDir16,0,0,NULL,&GenerateSubMenu,TextDup(FILEINFO(a->Data)->VPathName),m );
				a = a->Prev;
			}
			while ( a != b );
		}
		FreeList(l);
	}

	o->SubMenu = NewMenu(m);

}


void DesktopWidgetInsert ( PWidget o, PWidget i )
{
	WidgetInsert(o,i);

	if ( i->Type == ULONG_ID('w','i','n',' ') )
	{
		//DebugMessage("TaskbarAdd %x",i);
		TaskbarAdd(WINDOW(i));
	}
}

void DesktopWidgetSetFirstEx ( PWidget o, PWidget i  )
{
	WidgetSetFirstEx(o,i);

	if ( i->Type == ULONG_ID('w','i','n',' ') )
	{
		if (WINDOW(i)->WindowStates & WS_MINIMIZED)
		{
			WINDOW(i)->WindowStates &=~ WS_MINIMIZED;
		}

		TaskbarRedraw(WINDOW(i));
	}
}


void DesktopWidgetRemove ( PWidget o, PWidget i )
{
	WidgetRemove(o,i);

	if ( i->Type == ULONG_ID('w','i','n',' ') )
	{
		//DebugMessage("TaskbarRemove %x",i);
		TaskbarRemove(WINDOW(i));
	}
}

/**
*	Global variables
*/
PMenu Menu			= 0;
PTimer  ClockTimer	= 0;

l_bool AppEventHandler ( PWidget o, PEvent Ev )
{
	switch (Ev->Message)
	{
		case BTN_START:
		{
			TPoint p;
			PointAssign(&p,WIDGET(GSStart)->Absolute.a.x-15, WIDGET(GSStart)->Absolute.b.y+1);
			PopUpMenu(&Me, p, Menu, WIDGET(GSStart), 0);
			return true;
		}
		break;

		case IDM_QUIT:
		{

			if ( MessageBox(&Me, "Codename Phoenix", "Are you sure you want to exit oZone?", MBB_YESNO|MBI_QUESTION) == MSG_YES)
			{
				TEvent Event;
				EventAssign(&Event,EV_MESSAGE, MSG_QUIT, NULL, KState, Mouse->State);
	    		HandleEvent(&Event);

			}
			return true;
		}
		break;

		/**
		*	Start menu items
		*/
		case IDM_PROGRAMS:
		{
			FileRun(MENUITEM(Ev->Extra)->Args,NULL);
			return true;
		}
		break;

		case IDM_CP:
		{
			FileRun("xapps/nav.app","/Control Panel/");
			return true;
		}
		break;

		case IDM_ABOUT:
		{
			FileRun("xapps/about.app","about");
			return true;
		}
		break;
	}

	return false;
}

l_int Main (l_text Args)
{
	/**
	*	Export symbols
	*/
	APPEXPORT(TraybarAdd);
	APPEXPORT(TraybarRemove);
	APPEXPORT(WallpaperReLoad);
	//DebugMessage("ThemeLoad");


	/**
	 *	Get desktop theme
	 */
	ThemeLoad(KeyGetText("/USER/DESKTOP/theme", "./DESKTOP/plex.ini"));

	/**
	 *	Initialize panels
	 */
	WallpaperInit();
	GSPanelInit();
	Menu = NewMenu(
     NewMenuItemEx("Programs",NULL,0,0,NULL,&GenerateSubMenu,"SYSTEM/MENU",
     NewMenuItem( "Control Panel", NULL, IDM_CP, NULL, NULL,
     NewMenuItem( "Help and Support", NULL, NULL, NULL, NewMenu( //
     			NewMenuItem("Help", NULL, NULL, NULL, NULL,
     			NewMenuItem("Bug report", NULL, NULL, NULL, NULL,
     			NewMenuItem("About", NULL, IDM_ABOUT, NULL, NULL,
     			NULL)))),
     NewMenuItemSeparator(
     NewMenuItem( "Quit", LoadImage("SYSTEM/ICONS/power16.bmp"), IDM_QUIT, NULL, NULL,
     NULL)))))
	);

	ClockTimer = NewTimer(&Me, 1000*60, &RefreshPanelClock, NULL);

	Desktop->Insert = &DesktopWidgetInsert;
	Desktop->Remove = &DesktopWidgetRemove;
	Desktop->SetFirstEx = &DesktopWidgetSetFirstEx;

	return true;
}

void Close ( void )
{
	Desktop->Insert = &WidgetInsert;
	Desktop->Remove = &WidgetRemove;
	Desktop->SetFirstEx = &WidgetSetFirstEx;

	GSPanelDestroy();
	GSWallpaperDestroy();
	KillTimer(ClockTimer);
}

