#include "kernel.h"
#include "widget.h"
#include "window.h"
#include "canvas.h"

l_ulong UID			= ULONG_ID('I','M','M','G');
l_ulong AppVersion	= ULONG_ID(0,1,0,0);
char AppName[]		= "Immagine";
l_uid	nUID		= "app:immagine";
l_uid NeededLibs[]	= { "canvas","window","" };

PWindow w = 0;
PCanvas c = 0;
p_bitmap BMP = 0;

l_bool AppEventHandler ( PWidget o, PEvent Event )
{
	if ( Event->Type == EV_MESSAGE )
	{
		switch ( Event->Message )
		{
			case WM_CLOSE:
			{
				CloseApp(&Me);
				WidgetDispose(WIDGET(o));
				return true;
			}
			break;
		}
	}

	return false;
}

void Draw ( PWidget o, p_bitmap buffer, PRect w ){
	rectfill(buffer, o->Absolute.a.x, o->Absolute.a.y, o->Absolute.b.x, o->Absolute.b.y, makecol(0,0,0));

	if ( BMP ){
		draw_sprite( buffer, BMP, o->Absolute.a.x, o->Absolute.a.y);
	}
}

l_int Main (l_text Args)
{
	TRect r;
	TRect rr;

	if ( Args ){
		BMP = LoadData(TYPE_IMAGE, Args);
		if ( !BMP ) return false;
	} else
		return false;

	RectAssign(&r, 0, 0, BMP->w, BMP->h);
	w = CreateWindow(&Me, r, "Immagine", WF_NORMAL|WF_CENTERED);
	WIDGET(w)->AppEvHdl = &AppEventHandler;
	InsertWidget(WIDGET(DeskTop), WIDGET(w));

	RectAssign(&r,0,0, BMP->w, BMP->h);
	c = CreateCanvas(&Me, r);
	WIDGET(c)->Draw = &Draw;
	InsertWidget(WIDGET(w), WIDGET(c));

	WidgetDrawAll(WIDGET(w));

	WindowSetCaption(w,"Immagine - %s",Args);

	return true;
}

void Close (void)
{

}
