/**
*	(c) Copyright 2003,2004 Point Mad, Lukas Lipka. All rights reserved.
*
*	FILE:			font.c
*
*	PROJECT:		Phoenix engine - Core
*
*	DESCRIPTION:	Exetended font manipulation
*
*	CONTRIBUTORS:
*					Lukas Lipka
*
*	TODO:			@
*
*	BUG:			@
*
*	MISTAKE:		@
*
*	IDEA:			@
*/
#include "kernel.h"

/**
*	NAME: default_font
*	DESCRIPTION: Global font variable, which is used as the default system font.
*/
_PUBLIC FONT* default_font = 0;

/**
*	NAME: FontLoad
*	DESCRIPTION: Loads a font
*	RETURN: The loaded font, or default font on failure.
*/
_PUBLIC FONT*  FontLoad ( l_text szFilename )
{
	l_text Ext	= TextRChr(szFilename, '.');

	if (!Ext) return default_font;

	if (!fopen(szFilename, "rb")) return default_font;

	if (!stricmp(Ext, ".FNT")) return load_font(szFilename);
	else if (!stricmp(Ext, ".TTF"))	return load_ttf_font (szFilename, 9, 2);
	else return default_font;
}
